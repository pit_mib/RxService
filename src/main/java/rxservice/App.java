package rxservice;

import rxservice.data.NetworkDataSource;

public class App {
    public static void main(String[] args) {
        NetworkDataSource.getInstance().getWeatherDate().subscribe(System.out::println);
        NetworkDataSource.getInstance().getWeatherTwo().subscribe(System.out::println);
    }
}
