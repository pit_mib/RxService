package rxservice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class WeatherEntry {
    @SerializedName("dt")
    @Expose
    public Integer dt;
    @SerializedName("pressure")
    @Expose
    public Double pressure;
    @SerializedName("humidity")
    @Expose
    public Double humidity;
    @SerializedName("speed")
    @Expose
    public Double speed;
    @SerializedName("deg")
    @Expose
    public Integer deg;
    @SerializedName("clouds")
    @Expose
    public Integer clouds;

    public WeatherEntry(Integer dt, Double pressure, Double humidity, Double speed, Integer deg, Integer clouds) {
        this.dt = dt;
        this.pressure = pressure;
        this.humidity = humidity;
        this.speed = speed;
        this.deg = deg;
        this.clouds = clouds;
    }

    public String getDate() {
        LocalDateTime dateTime = LocalDateTime.ofEpochSecond(dt, 0, ZoneOffset.UTC);
        return dateTime.toString();
    }

    public Double getPressure() {
        return pressure;
    }

    public Double getHumidity() {
        return humidity;
    }

    public Double getSpeed() {
        return speed;
    }

    public Integer getDeg() {
        return deg;
    }

    public Integer getClouds() {
        return clouds;
    }
}
