package rxservice.data.github;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

// https://github.com/eugenp/tutorials/tree/master/libraries/src/main/java/com/baeldung/retrofit
public interface GitHubBasicApi {
    @GET("users/{user}/repos")
    Call<List> listRepos(@Path("user") String user);

    @GET("repos/{user}/{repo}/contributors")
    Call<List> listRepoContributors(
            @Path("user") String user,
            @Path("repo") String repo);
}
