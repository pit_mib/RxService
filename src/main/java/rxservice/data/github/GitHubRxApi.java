package rxservice.data.github;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rxservice.model.github.Contributor;
import rxservice.model.github.Repository;

import java.util.List;

public interface GitHubRxApi {
    @GET("users/{user}/repos")
    Observable<List<Repository>> listRepos(@Path("user") String user);

    @GET("repos/{user}/{repo}/contributors")
    Observable<List<Contributor>> listRepoContributors(
            @Path("user") String user,
            @Path("repo") String repo);
}
