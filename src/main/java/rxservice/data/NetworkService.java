package rxservice.data;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkService {
    private static final String BASE_URL = "https://andfun-weather.udacity.com/";
    private WeatherApi weatherApi;
	
	public NetworkService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        weatherApi = retrofit.create(WeatherApi.class);
    }
   
    public WeatherApi getWeatherApi() {
        return weatherApi;
    }
}
