package rxservice.data;

import com.google.gson.annotations.SerializedName;
import rxservice.model.WeatherEntry;

import java.util.List;

public class WeatherResponse {
    @SerializedName("list")
    private List<WeatherEntry> list;

    public List<WeatherEntry> getList() {
        return list;
    }

    public void setResults(List<WeatherEntry> list) {
        this.list = list;
    }
}
