package rxservice.data;

import io.reactivex.Observable;
import rxservice.model.WeatherEntry;

import java.util.List;

public class NetworkDataSource {
	private WeatherApi weatherApi;

	private static final Object LOCK = new Object();
	private static volatile NetworkDataSource sInstance;

	private NetworkDataSource() {
		NetworkService networkService = new NetworkService();
		weatherApi = networkService.getWeatherApi();
	}

	public static NetworkDataSource getInstance() {
		if (sInstance == null) {
			synchronized (LOCK) {
				if (sInstance == null) {
					sInstance = new NetworkDataSource();
				}
			}
		}
		return sInstance;
	}
	
	public void getWeather() {
		weatherApi.getForecast()
		.subscribe(s -> System.out.println(s));
	}

	public Observable<WeatherResponse> getWeatherTwo() {
		return weatherApi.getForecast();
	}
	
	public Observable<String> getWeatherDate() {
		return weatherApi.getForecast()
				.map(WeatherResponse::getList)
				.flatMapIterable(s -> s)
				.map(WeatherEntry::getDate);
	}
}
