package rxservice.data;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface WeatherApi {
    @GET("/weather")
    Observable<WeatherResponse> getForecast();
}
