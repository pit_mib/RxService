package rxservice.tryrx;

import io.reactivex.Observable;
import rxservice.model.WeatherEntry;

import java.util.Arrays;
import java.util.List;

public class TryRx {
    private static List<WeatherEntry> weatherEntryList = Arrays.asList(
            new WeatherEntry(1523359847, 1044.2105548675372, 92.1144956071528, 56.66593405093076, 148, 20),
            new WeatherEntry(1523446247, 1044.2105548675372, 92.1144956071528, 56.66593405093076, 148, 20),
            new WeatherEntry(1523532647, 1044.2105548675372, 92.1144956071528, 56.66593405093076, 148, 20),
            new WeatherEntry(1523619047, 1044.2105548675372, 92.1144956071528, 56.66593405093076, 148, 20),
            new WeatherEntry(1523705447, 1044.2105548675372, 92.1144956071528, 56.66593405093076, 148, 20),
            new WeatherEntry(1523791847, 1044.2105548675372, 92.1144956071528, 56.66593405093076, 148, 20)
    );

    public static void main(String[] args) {
        System.out.println(weatherEntryList.get(0).getDate());
        tryObserver();
    }

    public static void tryObserver() {
        Observable.fromArray(weatherEntryList)
                .subscribe(weatherEntries -> weatherEntries.forEach(weatherEntrie -> System.out.println(weatherEntrie.getDate())));
    }
}
